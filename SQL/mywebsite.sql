CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;

USE mywebsite;

CREATE TABLE user(
user_id int PRIMARY KEY AUTO_INCREMENT,
user_name varchar(255) NOT NULL,
login_id varchar(255) UNIQUE NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL
);

CREATE TABLE favorite_book(
favorite_id int PRIMARY KEY AUTO_INCREMENT,
book_id int NOT NULL,
user_id int NOT NULL,
create_date DATETIME NOT NULL
);

CREATE TABLE review_book(
review_id int PRIMARY KEY AUTO_INCREMENT,
favorite_id int NOT NULL,
user_id int NOT NULL,
review text
);

CREATE TABLE book(
book_id int PRIMARY KEY AUTO_INCREMENT,
book_title varchar(255) NOT NULL,
author_name varchar(255) NOT NULL,
publisher varchar(255) NOT NULL,
publish_year date NOT NULL,
detail text,
file_name varchar(255) NOT NULL,
category_id int NOT NULL
);

CREATE TABLE book_category(
category_id int PRIMARY KEY AUTO_INCREMENT,
category_name varchar(255) NOT NULL
);

INSERT INTO user VALUE(
1,
'管理者',
'admin',
'1992-10-22',
'password',
'2020-11-10',
'2020-11-10'
);

INSERT INTO book_category VALUE(
1,
'すべて'
);

INSERT INTO book VALUE(
10,
'スッキリわかるSQL入門 第2版 ドリル222問付き!',
'中山清喬',
'インプレス',
'2018-11-30',
'データベースとSQLをやさしく、しっかり学べる。開発現場で困らないDBMSの「方言」リファレンスと、初心者が悩むエラーの原因がすぐわかる「エラー解決 虎の巻」付き!',
'sql_nyuumon.jpg',
1
);

SELECT * FROM review_book JOIN favorite_book
ON review_book.favorite_id = favorite_book.favorite_id JOIN book
ON favorite_book.book_id = book.book_id
WHERE review_book.user_id =2;


DELETE FROM user WHERE login_id = 1;
