<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザー情報</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<h3 font-weight="bold" style="text-align: center">ユーザー情報</h3>

		<hr>
		<table align="center">
			<tr>
				<th>ログインID</th>
				<th>ユーザ名</th>
				<th>生年月日</th>
				<th></th>
			</tr>
			<tr>
				<td>${userInfo.loginId}</td>
				<td>${userInfo.name}</td>
				<td>${userInfo.birthDate}</td>
				<td><a class="btn btn-success"
					href="UserUpdate?id=${userInfo.id}">更新</a></td>
			</tr>
		</table>

		<hr>


		<!--レビューしたものを繰り返し表示する -->
		<table align="center">
			<tr>
				<th width="100">著者名</th>
				<th width="200">書籍名</th>
				<th width="300">レビュー</th>
			</tr>
			<c:forEach var="review" items="${reviewList}">
				<tr>
					<td>${review.authorName}</td>
					<td>${review.title}</td>
					<td>${review.review}</td>
				</tr>
			</c:forEach>
		</table>

		<p align="right"><a href="Index">TOPページ</a></p>


	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>

</html>