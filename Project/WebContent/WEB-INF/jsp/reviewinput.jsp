<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>レビュー入力</title>
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">

		<h3 font-weight="bold" align="center">レビュー入力画面</h3>
		<div align="center">
			<form action="Review" method="post">
				<div align="center" class="borderd">
					<img src="img/${book.fileName}">
				</div>
				<p>
					レビュー:
					<textarea name="review" required></textarea>
				</p>
		</div>
		<div class="row">
			<table align="center">
				<tr>


					<td><input type="hidden" value="${book.id}" name="bookId">
						<button type="submit" name="review"
							style="background-color: black; color: white">レビュー</button>
						</form>
						</td>
						<form>
							<td><input type="button" value="戻る" onclick="history.back()"
								style="background-color: black; color: white">
						</form></td>

				</tr>
			</table>
		</div>


	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>


</html>