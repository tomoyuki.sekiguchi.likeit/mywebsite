<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>書籍詳細</title>
<link href="css/style.css" rel="stylesheet">

</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="container">
		<table align="center">
			<tr>
				<td width="250px" align="center">
					<form>
						<input type="button" value="戻る" onclick="history.back()"
							style="background-color: black; color: white">
					</form>
				</td>
				<td width="250px" align="center"><h3 font-weight="bold"
						align="center">書籍詳細情報</h3></td>
				<td width="250px" align="center">
					<form action="FavoriteAdd" method="get">
						<input type="hidden" name="book_id" value="${book.id}">
						<button type="submit" name="confirm_button" name="favorite"
							style="background-color: black; color: white">お気に入りリストに追加</button>
					</form>
				</td>
			</tr>
		</table>
		<br> <br>
		<!--書籍の画像・情報を読み取る-->
		<div class="row">
			<div class="image">

				<img src="img/${book.fileName}">
			</div>


			<div class="text">
				<h4>${book.authorName}</h4>
				<h4>${book.title}</h4>
				<p>${book.detail}</p>
			</div>
		</div>

		<p align="right">
			<a href="Index">TOPページ</a>
		</p>


	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>


</html>