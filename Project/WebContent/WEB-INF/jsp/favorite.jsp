<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>お気に入りリスト</title>
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<h3 font-weight="bold" align="center">お気に入りリスト</h3>
		<div class="section">
			<table align="center">
				<thead>
					<tr>
						<th class="center" width="100">著者名</th>
						<th class="center" width="200">書籍名</th>
						<th class="center" width="100">出版社名</th>
						<th class="center" width="100">出版年</th>
						<th class="center"></th>
					</tr>
				</thead>
				<tbody>
					<!--以下、追加済みのお気に入りが続く-->
					<c:forEach var="book" items="${favoriteBook}">
						<tr>
							<th class="center" width="100">${book.authorName}</th>
							<th class="center" width="200">${book.title}</th>
							<th class="center" width="100">${book.publisher}</th>
							<th class="center" width="100">${book.publishYear}</th>
							<!-- ここに削除・レビューボタンを設置する -->
							<th>
							<a href="Review?bookId=${book.id}" >レビュー</a>
							<a href="DeleteFavoriteBook?bookId=${book.id}">削除</a>
							</th>
						</tr>
					</c:forEach>
				</tbody>

			</table>
		</div>
		<!--戻るボタン-->
		<div align="right">
			<form>
				<input type="button" value="戻る" onclick="history.back()"
					style="background-color: black; color: white">
			</form>
		</div>
		<p align="right"><a href="Index">TOPページ</a></p>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>


</html>