<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録完了</title>
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<h3 font-weight="bold" style="text-align: center">登録完了</h3>

	<div align="center">
		<table>
			<tr>
				<th>名前:</th>
				<td><input type="text" value="${udb.name}" readonly></td>
			</tr>

			<tr>
				<th>ログインID:</th>
				<td><input type="text" value="${udb.loginId}"  readonly></td>
			</tr>

			<tr>
				<th>生年月日:</th>
				<td><input type="text" value="${udb.birthDate}" readonly></td>
			</tr>
		</table>
		<div>
			<p>上記の内容で登録しました。</p>
		</div>
		<a href="Login" style="color: blue;">ログイン</a>

	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>