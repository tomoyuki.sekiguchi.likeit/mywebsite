<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>お気に入り登録確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<h3 font-weight="bold" align="center">登録確認画面</h3>
		<div class="section">
			<div align="center" class="borderd">
				<img src="img/${book.fileName}">
			</div>
			<p align="center">上記の書籍をお気に入りに登録します。</p>
			<div align="center">
				<form action="FavoriteAdd" method="post">

					<input type="hidden" value="${book.id}" name="bookId">
					<button class="btn btn-primary" type="submit" name="action"
						style="background-color: black; color: white">登録</button>

				</form>
			</div>
			<p align="right"><a href="Index">TOPページ</a></p>

		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>


</html>