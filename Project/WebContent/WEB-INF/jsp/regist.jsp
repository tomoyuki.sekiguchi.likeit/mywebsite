<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<h3 font-weight="bold" style="text-align: center">新規登録</h3>

	<c:if test="${errMsg != null}">
					<p class="red-text center-align">${errMsg}</p>
					<br>
				</c:if>

	<div align="center">
		<form action="RegistConfirm" method="post">
			<table>
				<tr>
					<th>名前:</th>
					<td><input type="text" name="user_name" required></td>
				</tr>

				<tr>
					<th>ログインID:</th>
					<td><input type="text" name="login_id" required></td>
				</tr>

				<tr>
					<th>生年月日:</th>
					<td><input type="text" name="birth_date" required></td>
				</tr>

				<tr>
					<th>パスワード:</th>
					<td><input type="password" name="password" required></td>
				</tr>

				<tr>
					<th>パスワード(確認):</th>
					<td><input type="password" name="confirm_password" required></td>
				</tr>
			</table>
			<div>
				<button type="submit" name="action"
					style="background-color: black; color: white">確認</button>
			</div>
		</form>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>


</html>