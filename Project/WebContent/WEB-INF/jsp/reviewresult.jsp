<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>レビュー完了</title>
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<h3 font-weight="bold" style="text-align: center">レビューが完了しました</h3>

	<div class="row" align="center">
		<a href="Index" class="btn btn-large waves-effect waves-light  col s8 offset-s2">TOPページへ</a>

	</div>

	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>


</html>