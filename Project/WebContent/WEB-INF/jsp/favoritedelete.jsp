<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>お気に入り削除</title>
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<h3>お気に入り削除確認</h3>


	<div class="form-group row">
		<label for="inputID" class="col-sm-2 col-form-label">${favoriteDelete.title}</label>

	</div>
	<p>を本当に削除してよろしいでしょうか。</p>
	<div class="decision">
		<form action="FavoriteList" method="get">
			<button type="submit" style="background-color: black; color: white"
				name="cancel">キャンセル</button>
		</form>
		<form action="DeleteFavoriteBook" method="post">
			<input type="hidden" value="${favoriteDelete.id}" name="id">
			<button type="submit" style="background-color: black; color: white"
				name="ok">OK</button>
		</form>
		<p align="right"><a href="Index">TOPページ</a></p>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>