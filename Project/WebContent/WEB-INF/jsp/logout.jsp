<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログアウト</title>
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container" align="center">
		<h3>ログアウトしました</h3>
		<p class="center-align">
			<a href="Login">ログイン画面へ</a>
		</p>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>