<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録/入力内容確認</title>
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<h3 font-weight="bold" style="text-align: center">入力内容確認</h3>

	<div align="center">
		<form action="RegistResult" method="post">
			<table>
				<tr>
					<th>名前:</th>
					<td><input type="text" name="user_name" value="${udb.name}" readonly></td>
				</tr>

				<tr>
					<th>ログインID:</th>
					<td><input type="text" name="login_id"  value="${udb.loginId}"  readonly></td>
				</tr>

				<tr>
					<th>生年月日:</th>
					<td><input type="text" name="birth_date"  value="${udb.birthDate}"  readonly></td>
				</tr>

				<tr>
					<th>パスワード:</th>
					<td><input type="password" name="password" value="${udb.password}" readonly></td>
				</tr>
			</table>
			<div>
				<p>上記の内容で登録してよろしいでしょうか？</p>
			</div>
			<table>
				<tr>
					<td><button type="button" name="fix_button" onclick="history.back()" name="cancel"
							style="background-color: black; color: white">修正</button></td>
					<td><button type="submit" name="confirm_button" name="regist"
							style="background-color: black; color: white">登録</button></td>
				</tr>
			</table>
		</form>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>


</html>