<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>TOPページ</title>
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<h1 font-weight="bold" style="text-align: center">
			<b>ブックリストサイト</b>
		</h1>
		<div align="center">
			<h5>記録をつけて快適な読書ライフ</h5>

			<form action="BookSearchResult" method="get">
				<button style="background-color: black; color: white">検索</button> <input type="text"
					name="search_word"> <select class="category">
					<option value="cat1">すべて</option>
					<option value="cat1">人文</option>
					<option value="cat1">科学</option>
				</select>
			</form>
			<br> <br>
		</div>
	</div>

	<div class="container">
		<div align="center">
			<div>
				<h5>おすすめの書籍一覧</h5>
			</div>
			<div class="section">
				<div>
					<c:forEach var="book" items="${bookList}">
						<div>
							<div class="card">
								<div class="card-imaage">
									<a href="Book?book_id=${book.id}"><img src="img/${book.fileName}"></a>
								</div>
								<div class="card-content">
									<span class="card-title">${book.title}</span>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>

		</div>
	</div>

	<jsp:include page="/baselayout/footer.jsp" />

</body>

</html>