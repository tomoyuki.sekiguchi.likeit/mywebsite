<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>検索結果</title>
<link href="css/booksearchresult.css" rel="stylesheet">
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<div align="center">
			<form action="BookSearchResult" method="get">
				<button style="background-color: black; color: white">検索</button><input type="text"
					name="search_word"> <select class="category">
					<option value="cat1">すべて</option>
					<option value="cat1">人文</option>
					<option value="cat1">科学</option>

				</select>
			</form>
		</div>
	</div>

	<div class="container">
		<div align="center">
			<div>
				<h3>検索結果</h3>
			</div>
			<div class="section">
				<!--書籍情報-->
				<div>
					<c:forEach var="book" items="${bookList}">
						<div>
							<div class="card">
								<div class="card-image">
									<a href="Book?book_id=${book.id}"><img
										src="img/${book.fileName}"></a>
								</div>
								<div class="card-content">
									<span class="card-title">${book.title}</span>
									<p>${book.authorName}</p>
								</div>
							</div>
						</div>
					</c:forEach>

				</div>
			</div>
			<div class="row"></div>
		</div>
		<div class="row center">
			<ul class="pagination" style="list-style:none;">
				<!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li class="disabled"><a><i class="material-icons">前へ</i></a></li>
					</c:when>
					<c:otherwise>
						<li class="waves-effect"><a href="BookSearchResult?search_word=${searchWord}&page_num=${pageNum - 1}"><i class="material-icons">前へ</i></a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li <c:if test="${pageNum == status.index }"> class="active"</c:if>><a href="BookSearchResult?search_word=${searchWord}&page_num=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<li class="disabled"><a><i class="material-icons">次へ</i></a></li>
				</c:when>
				<c:otherwise>
					<li class="waves-effect"><a href="BookSearchResult?search_word=${searchWord}&page_num=${pageNum + 1}"><i class="material-icons">次へ</i></a></li>
				</c:otherwise>
				</c:choose>
			</ul>
		</div>
	</div>


	<jsp:include page="/baselayout/footer.jsp" />
</body>

</html>