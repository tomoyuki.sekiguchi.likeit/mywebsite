<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />
	<h3 font-weight="bold" style="text-align: center">ユーザー情報更新</h3>

	<div align="center">
		<form action="UserUpdate" method="post">
			<table>
				<tr>
					<th>ログインID:</th>
					<td>${userInfo.loginId}<input type="hidden" value=" ${userInfo.id}" name="login_id"></td>
				</tr>

				<tr>
					<th>名前:</th>
					<td><input type="text" name="user_name" required></td>
				</tr>


				<tr>
					<th>生年月日:</th>
					<td><input type="text" name="birth_date" required></td>
				</tr>

				<tr>
					<th>パスワード:</th>
					<td><input type="password" name="password" required></td>
				</tr>

				<tr>
					<th>パスワード(確認):</th>
					<td><input type="password" name="confirm_password" required></td>
				</tr>
			</table>
			<div>
				<button type="submit" name="action"
					style="background-color: black; color: white">更新</button>
		</form>

		<button type="button" onclick="history.back()" name="back"
			style="background-color: black; color: white">戻る</button>

		<p align="right"><a href="Index">TOPページ</a></p>
	</div>

	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>


</html>