<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
</head>

<body>
	<jsp:include page="/baselayout/header.jsp" />
	<div class="container">
		<h3 font-weight="bold" style="text-align: center">ログイン画面</h3>
		<form action="LoginResult" method="post">
			<div style="text-align: center">
				<c:if test="${loginErrorMessage != null}">
					<p class="red-text center-align">${loginErrorMessage}</p>
					<br>
				</c:if>
				<label>ログインID:</label> <input type="text" name="login_id">
			</div>

			<div style="text-align: center">
				<label>パスワード:</label> <input type="password" name="password">
			</div>

			<br>

			<div style="text-align: center">
				<button type="submit" name="action"
					style="background-color: black; color: white">ログイン</button>
			</div>

			<div class="row">
				<div class="col s8 offset-s2">
					<p align="center">
						<a href="Regist">新規登録</a>
					</p>
				</div>
			</div>

		</form>

	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>

</html>