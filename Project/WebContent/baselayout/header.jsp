<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ヘッダー</title>
</head>

<body>
    <header style="background-color: black ; display: flex; align-items: center">
    <h1 class="title" style="color: white ;float: left">ブックレビュー</h1>

      <ul class="right" style="color: white ;list-style: none">
      <% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>
      <%if(isLogin){ %>
        <li style="display: inline"><form action="FavoriteList" method="get"><button type="submit" style="color: white">お気に入り</button></form></li>
        <li style="display: inline"><form action="UserInfo" method="get"><button type="submit" style="color: white">ユーザー情報</button></form></li>
        <%} %>
        <%if(isLogin){ %>
         <li style="display: inline"><a href="Logout" style="color: white">ログアウト</a></li>
         <%}else{ %>
         <li style="display: inline"><a href="Login" style="color: white">ログイン</a></li>
         <%} %>
      </ul>

    </header>

</body>

</html>