package beans;

import java.io.Serializable;

public class UserDataBeans implements Serializable{

	private int id;
	private String name;
	private String loginId;
	private String birthDate;
	private String password;

	public UserDataBeans() {
		this.name = "";
		this.loginId = "";
		this.birthDate = "";
		this.password = "";
	}
	public UserDataBeans(int userID, String name2, String loginId2, String birthDate2) {
		id = userID;
		name = name2;
		loginId = loginId2;
		birthDate = birthDate2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
