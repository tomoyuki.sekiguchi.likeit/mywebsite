package beans;

import java.io.Serializable;
import java.sql.Date;

public class FavoriteDataBeans implements Serializable {
	private int id;
	private int bookId;
	private int userId;
	private Date createDate;

	public FavoriteDataBeans(int favoriteId, int bookId2, int uId, java.util.Date createDate2) {
		id = favoriteId;
		bookId = bookId2;
		userId = uId;
		createDate = (Date) createDate2;
	}

	public FavoriteDataBeans() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
