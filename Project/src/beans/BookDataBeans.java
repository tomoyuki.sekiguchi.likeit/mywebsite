package beans;

import java.io.Serializable;

public class BookDataBeans implements Serializable{
		private int id;
		private String title;
		private String authorName;
		private String publisher;
		private String publishYear;
		private String detail;
		private String fileName;
		private int categoryId;


		public BookDataBeans(int bookId2, String title2, String authorName2, String publisher2, String publishYear2,
				String detail2, String fileName2, int categoryId2) {
			id = bookId2;
			title = title2;
			authorName=authorName2;
			publisher=publisher2;
			publishYear=publishYear2;
			detail=detail2;
			fileName=fileName2;
			categoryId=categoryId2;
		}


		public BookDataBeans() {
			// TODO 自動生成されたコンストラクター・スタブ
		}


		public String getTitle() {
			return title;
		}


		public void setTitle(String title) {
			this.title = title;
		}


		public String getAuthorName() {
			return authorName;
		}


		public void setAuthorName(String authorName) {
			this.authorName = authorName;
		}


		public String getPublisher() {
			return publisher;
		}


		public void setPublisher(String publisher) {
			this.publisher = publisher;
		}


		public String getPublishYear() {
			return publishYear;
		}


		public void setPublishYear(String publishYear) {
			this.publishYear = publishYear;
		}


		public String getDetail() {
			return detail;
		}


		public void setDetail(String detail) {
			this.detail = detail;
		}


		public String getFileName() {
			return fileName;
		}


		public void setFileName(String fileName) {
			this.fileName = fileName;
		}


		public int getCategoryId() {
			return categoryId;
		}


		public void setCategoryId(int categoryId) {
			this.categoryId = categoryId;
		}


		public void setId(int id) {
			this.id = id;
		}


		public int getId() {
			return id;
		}

}
