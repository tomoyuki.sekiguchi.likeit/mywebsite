package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ReviewDataBeans;
import beans.UserDataBeans;
import dao.ReviewDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserInfo
 */
@WebServlet("/UserInfo")
public class UserInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

		//user情報を取得する
		int userId = (int)session.getAttribute("userId");
		UserDataBeans udb = UserDAO.findById(userId);

		request.setAttribute("userInfo", udb);


		//userIdを基にレビュー情報を取得する
		List<ReviewDataBeans> reviewList = ReviewDAO.findById(userId);

		request.setAttribute("reviewList",reviewList);



		//userdata.jspへフォワードする
		RequestDispatcher dispatcher = request.getRequestDispatcher(Helper.USER_DATA_PAGE);
		dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
