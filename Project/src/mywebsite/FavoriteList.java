package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BookDataBeans;
import dao.FavoriteDAO;

/**
 * Servlet implementation class FavoriteList
 */
@WebServlet("/FavoriteList")
public class FavoriteList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//お気に入りリストを表示するためのサーブレット
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {
		int userId = (int)session.getAttribute("userId");

		//idでfavoritebookを探す→リスト型に変更する
		List<BookDataBeans> favoriteBook = FavoriteDAO.findFavoriteBooksById(userId);

		request.setAttribute("favoriteBook", favoriteBook);

		//favorite.jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(Helper.FAVORITE_PAGE);
		dispatcher.forward(request, response);


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
