package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BookDataBeans;
import dao.FavoriteDAO;

/**
 * Servlet implementation class DeleteFavoriteBook
 */
@WebServlet("/DeleteFavoriteBook")
public class DeleteFavoriteBook extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {

		int bookId =Integer.parseInt(request.getParameter("bookId"));

		BookDataBeans bdb = FavoriteDAO.findByBookId(bookId);

		request.setAttribute("favoriteDelete", bdb);

		RequestDispatcher dispatcher = request.getRequestDispatcher(Helper.FAVORITE_DELETE_PAGE);
		dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

		int bookId =Integer.parseInt(request.getParameter("id"));


		FavoriteDAO favoriteDao = new FavoriteDAO();
		favoriteDao.delete(bookId);

		//削除後、お気に入りリストへリダイレクト
		response.sendRedirect("FavoriteList");

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

}
