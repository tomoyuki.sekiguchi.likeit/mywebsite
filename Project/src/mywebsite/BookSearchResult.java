package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BookDataBeans;
import dao.BookDAO;

/**
 * Servlet implementation class BookSearchResult
 */
@WebServlet("/BookSearchResult")
public class BookSearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	final static int PAGE_MAX_ITEM_COUNT = 3;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			String searchWord = request.getParameter("search_word");

			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer
					.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			session.setAttribute("searchWord", searchWord);

			ArrayList<BookDataBeans> searchResult = BookDAO.searchBook(searchWord, pageNum, PAGE_MAX_ITEM_COUNT);

			// 検索ワードに対しての総ページ数を取得
			double itemCount = BookDAO.getCount(searchWord);
			int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

			//総アイテム数
			request.setAttribute("itemCount", (int) itemCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("bookList", searchResult);

			//フォワード
			request.getRequestDispatcher(Helper.SEARCH_RESULT_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

}
