package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;

/**
 * Servlet implementation class RegistConfirm
 */
@WebServlet("/RegistConfirm")
public class RegistConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		try {

		String loginId = request.getParameter("login_id");
		String userName = request.getParameter("user_name");
		String birthDate = request.getParameter("birth_date");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirm_password");

		//情報をセットする
		UserDataBeans udb = new UserDataBeans();
		udb.setLoginId(loginId);
		udb.setName(userName);
		udb.setBirthDate(birthDate);
		udb.setPassword(password);

		//パスワードと確認用パスワードが同じか。
		if(!password.equals(confirmPassword)){
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.getRequestDispatcher(Helper.REGIST_PAGE).forward(request, response);
		}else if(loginId.equals("") ||userName.equals("") ||birthDate.equals("") ||password.equals("")) {
			//if else文に変更する？

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.getRequestDispatcher(Helper.REGIST_PAGE).forward(request, response);

		//上記の条件でなければ、登録確認ページへ
		}else {
			request.setAttribute("udb", udb);
			request.getRequestDispatcher(Helper.REGIST_CONFIRM_PAGE).forward(request, response);
		}





		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
