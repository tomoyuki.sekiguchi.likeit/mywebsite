package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class RegistResult
 */
@WebServlet("/RegistResult")
public class RegistResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		try {

		String loginId = request.getParameter("login_id");
		String userName = request.getParameter("user_name");
		String birthDate = request.getParameter("birth_date");
		String password = request.getParameter("password");


		UserDataBeans udb = new UserDataBeans();


		udb.setLoginId(loginId);
		udb.setName(userName);
		udb.setBirthDate(birthDate);
		udb.setPassword(password);
		//登録ボタンをクリックしたときの処理
		UserDAO.insertUser(udb);

		request.setAttribute("udb",udb);

		//登録完了画面にフォワード
		request.getRequestDispatcher(Helper.REGIST_RESULT_PAGE).forward(request,response);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
