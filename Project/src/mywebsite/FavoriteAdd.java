package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BookDataBeans;
import beans.FavoriteDataBeans;
import dao.BookDAO;
import dao.FavoriteDAO;

/**
 * Servlet implementation class FavoriteAdd
 */
@WebServlet("/FavoriteAdd")
public class FavoriteAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			int id = Integer.parseInt(request.getParameter("book_id"));
			int userId = (int)session.getAttribute("userId");

			//ここでDAOを使う？リスト型にfindAll？
			FavoriteDataBeans fdb = FavoriteDAO.findFavoriteId(id,userId);

			request.setAttribute("fdb",fdb);

			//fdb!=nullで定義する？
			if(fdb != null) {
				request.setAttribute("errMsg", "すでに登録されています");
				request.getRequestDispatcher(Helper.BOOK_PAGE).forward(request, response);
			}else {

			BookDataBeans book = BookDAO.findByBookID(id);

			request.setAttribute("book",book);

			//フォワード
			request.getRequestDispatcher(Helper.FAVORITE_CONFIRM_PAGE).forward(request, response);
			}


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {
			//ここでNULLになってしまう？
			String bookId = request.getParameter("bookId");
			int bid = Integer.parseInt(bookId);


			int userId = (int)session.getAttribute("userId");

		    FavoriteDAO.insertData(bid,userId);



			//FavoriteListリダイレクトへ
			response.sendRedirect("FavoriteList");

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
