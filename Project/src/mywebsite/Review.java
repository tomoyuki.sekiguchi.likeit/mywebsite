package mywebsite;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BookDataBeans;
import beans.FavoriteDataBeans;
import dao.BookDAO;
import dao.FavoriteDAO;
import dao.ReviewDAO;

/**
 * Servlet implementation class Review
 */
@WebServlet("/Review")
public class Review extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//book_idで本の情報を取得する
			int id = Integer.parseInt(request.getParameter("bookId"));

			BookDataBeans book = BookDAO.findByBookID(id);

			request.setAttribute("book", book);

		//reviewInputへフォワード
			request.getRequestDispatcher(Helper.REVIEW_INPUT_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		try {
			String bookId = request.getParameter("bookId");
			int bid = Integer.parseInt(bookId);

			int userId = (int)session.getAttribute("userId");

			//userid,bookidでfavoriteidを探す
			FavoriteDataBeans fdb = FavoriteDAO.findFavoriteId(bid,userId);

			request.setAttribute("fdb",fdb);

			//DAOから取得したデータを基にfavoriteidを取得する
			int fId = fdb.getId();

			String review = request.getParameter("review");

			//書籍のIDを基にレビューテーブルにINSERTする。
			ReviewDAO.insertReviewData(fId,userId,review);


		//reviewResultへフォワード
		request.getRequestDispatcher(Helper.REVIEW_RESULT_PAGE).forward(request, response);


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
