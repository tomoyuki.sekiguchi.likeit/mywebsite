package mywebsite;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpSession;

public class Helper {
	//それぞれのjspを登録する
		// 検索結果
		static final String SEARCH_RESULT_PAGE = "/WEB-INF/jsp/booksearchresult.jsp";
		// 書籍ページ
		static final String BOOK_PAGE = "/WEB-INF/jsp/book.jsp";
		// TOPページ
		static final String TOP_PAGE = "/WEB-INF/jsp/index.jsp";
		// エラーページ
		static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
		// レヴュー入力ページ
		static final String REVIEW_INPUT_PAGE = "/WEB-INF/jsp/reviewinput.jsp";
		// レヴュー完了ページ
		static final String REVIEW_RESULT_PAGE = "/WEB-INF/jsp/reviewresult.jsp";
		// お気に入り
		static final String FAVORITE_PAGE = "/WEB-INF/jsp/favorite.jsp";
		// お気に入り確認
		static final String FAVORITE_CONFIRM_PAGE = "/WEB-INF/jsp/favoriteconfirm.jsp";
		//お気に入り削除
		static final String FAVORITE_DELETE_PAGE = "/WEB-INF/jsp/favoritedelete.jsp";
		// ユーザー情報
		static final String USER_DATA_PAGE = "/WEB-INF/jsp/userdata.jsp";
		// ユーザー情報更新
		static final String USER_DATA_UPDATE_PAGE = "/WEB-INF/jsp/userdataupdate.jsp";
		// レビュー履歴
		static final String USER_REVIEW_HISTORY_DETAIL_PAGE = "/WEB-INF/jsp/reviewhistorydetail.jsp";
		// ログイン
		static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
		// ログアウト
		static final String LOGOUT_PAGE = "/WEB-INF/jsp/logout.jsp";
		// 新規登録
		static final String REGIST_PAGE = "/WEB-INF/jsp/regist.jsp";
		// 新規登録入力内容確認
		static final String REGIST_CONFIRM_PAGE = "/WEB-INF/jsp/registconfirm.jsp";
		// 新規登録完了
		static final String REGIST_RESULT_PAGE = "/WEB-INF/jsp/registresult.jsp";

		public static Helper getInstance() {
			return new Helper();
		}


		/**
		 * ハッシュ関数
		 *
		 * @param target
		 * @return
		 */
		public static String getSha256(String target) {
			MessageDigest md = null;
			StringBuffer buf = new StringBuffer();
			try {
				md = MessageDigest.getInstance("SHA-256");
				md.update(target.getBytes());
				byte[] digest = md.digest();

				for (int i = 0; i < digest.length; i++) {
					buf.append(String.format("%02x", digest[i]));
				}
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			return buf.toString();
		}

		/**
		 * セッションから指定データを取得（削除も一緒に行う）
		 *
		 * @param session
		 * @param str
		 * @return
		 */
		public static Object cutSessionAttribute(HttpSession session, String str) {
			Object test = session.getAttribute(str);
			session.removeAttribute(str);

			return test;
		}

		/**
		 * ログインIDのバリデーション
		 *
		 * @param inputLoginId
		 * @return
		 */
		public static boolean isLoginIdValidation(String inputLoginId) {
			// 英数字アンダースコア以外が入力されていたら
			if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
				return true;
			}

			return false;

		}

}
