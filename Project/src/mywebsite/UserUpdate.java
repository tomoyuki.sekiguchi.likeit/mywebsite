package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//userIdを基にユーザ情報を取得する
			int userId = (int)session.getAttribute("userId");
			UserDataBeans udb = UserDAO.findById(userId);

			request.setAttribute("userInfo", udb);


			//userdataupdate.jspへフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher(Helper.USER_DATA_UPDATE_PAGE);
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {
			String id = request.getParameter("login_id");
			String name = request.getParameter("user_name");
			String birthDate = request.getParameter("birth_date");
			String password = request.getParameter("password");
			String confirmPassword = request.getParameter("confirm_password");

			if (!password.equals(confirmPassword)) {


							request.setAttribute("errMsg", "入力された内容は正しくありません");

							RequestDispatcher dispatcher = request.getRequestDispatcher(Helper.USER_DATA_UPDATE_PAGE);
							dispatcher.forward(request, response);
							return;
			}else if(name.equals("") ||birthDate.equals("")) {
							request.setAttribute("errMsg", "入力された内容は正しくありません");

							RequestDispatcher dispatcher = request.getRequestDispatcher(Helper.USER_DATA_UPDATE_PAGE);
							dispatcher.forward(request, response);
							return;
			}else {

				UserDAO.update(id,name,birthDate,password,confirmPassword);

				response.sendRedirect("UserInfo");
			}


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

}
