package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBManager;
import beans.BookDataBeans;
import beans.FavoriteDataBeans;

public class FavoriteDAO {
	public static void insertData(int bid, int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO favorite_book(book_id,user_id,create_date) VALUES(?,?,NOW())");
			st.setInt(1, bid);
			st.setInt(2, userId);
			st.executeUpdate();

			System.out.println("insertData completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static List<BookDataBeans> findFavoriteBooksById(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			List<BookDataBeans> favoriteBook = new ArrayList<BookDataBeans>();
			//ここでテーブル結合する？
			st = con.prepareStatement("SELECT * FROM favorite_book"
					+ " JOIN book"
					+ " ON favorite_book.book_id = book.book_id"
					+ " WHERE favorite_book.user_id = ?");

			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				BookDataBeans bdb = new BookDataBeans();
				bdb.setId(rs.getInt("book_id"));
				bdb.setTitle(rs.getString("book_title"));
				bdb.setAuthorName(rs.getString("author_name"));
				bdb.setPublisher(rs.getString("publisher"));
				bdb.setPublishYear(rs.getString("publish_year"));
				bdb.setDetail(rs.getString("detail"));
				bdb.setFileName(rs.getString("file_name"));
				bdb.setCategoryId(rs.getInt("category_id"));

				favoriteBook.add(bdb);
			}

			System.out.println("Find FavoriteList completed");

			return favoriteBook;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static BookDataBeans findByBookId(int bookId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM book WHERE book_id = ?");

			st.setInt(1, bookId);

			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int bookId2 = rs.getInt("book_id");
			String title = rs.getString("book_title");
			String authorName = rs.getString("author_name");
			String publisher = rs.getString("publisher");
			String publishYear = rs.getString("publish_year");
			String detail = rs.getString("detail");
			String fileName = rs.getString("file_name");
			int categoryId = rs.getInt("category_id");
			BookDataBeans bdb = new BookDataBeans(bookId2, title, authorName, publisher, publishYear, detail, fileName,
					categoryId);

			System.out.println(bdb);

			return bdb;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public void delete(int bookId) {
		Connection con = null;

		try {
			con = DBManager.getConnection();

			String sql = "DELETE FROM favorite_book WHERE book_id=?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, bookId);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public static FavoriteDataBeans findFavoriteId(int bid, int userId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM favorite_book WHERE book_id = ? AND user_id= ?");

			st.setInt(1, bid);
			st.setInt(2,userId);


			ResultSet rs = st.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int favoriteId = rs.getInt("favorite_id");
			int bookId = rs.getInt("book_id");
			int uId = rs.getInt("user_id");
			Date createDate = rs.getDate("create_date");
			FavoriteDataBeans fdb = new FavoriteDataBeans(favoriteId,bookId,uId,createDate);

			System.out.println(fdb);

			return fdb;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

}
