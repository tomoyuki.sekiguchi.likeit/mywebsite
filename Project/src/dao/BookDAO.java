package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.BookDataBeans;

public class BookDAO {
	//ランダムで書籍を取得するメソッド
	public static ArrayList<BookDataBeans> getRandBook(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM book ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<BookDataBeans> bookList = new ArrayList<BookDataBeans>();

			while (rs.next()) {
				BookDataBeans book = new BookDataBeans();
				book.setId(rs.getInt("book_id"));
				book.setTitle(rs.getString("book_title"));
				book.setAuthorName(rs.getString("author_name"));
				book.setPublisher(rs.getString("publisher"));
				book.setPublishYear(rs.getString("publish_year"));
				book.setDetail(rs.getString("detail"));
				book.setFileName(rs.getString("file_name"));
				book.setCategoryId(rs.getInt("category_id"));
				bookList.add(book);
			}
			System.out.println("getRandBook completed");
			return bookList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//検索用の処理。書籍名か著者名検索
	public static ArrayList<BookDataBeans> searchBook(String searchWord, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			if (searchWord.length() == 0) {
				// 全検索
				st = con.prepareStatement("SELECT * FROM book ORDER BY id ASC LIMIT ?,? ");
				st.setInt(1, startiItemNum);
				st.setInt(2, pageMaxItemCount);
			} else {
			st = con.prepareStatement(
					"SELECT * FROM book WHERE book_title LIKE ? OR author_name LIKE ? ORDER BY book_id ASC LIMIT ?,? ");
			st.setString(1, "%" + searchWord + "%");
			st.setString(2, "%" + searchWord + "%");
			st.setInt(3, startiItemNum);
			st.setInt(4, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<BookDataBeans> bookList = new ArrayList<BookDataBeans>();

			while (rs.next()) {
				BookDataBeans book = new BookDataBeans();
				book.setId(rs.getInt("book_id"));
				book.setTitle(rs.getString("book_title"));
				book.setAuthorName(rs.getString("author_name"));
				book.setPublisher(rs.getString("publisher"));
				book.setPublishYear(rs.getString("publish_year"));
				book.setDetail(rs.getString("detail"));
				book.setFileName(rs.getString("file_name"));
				book.setCategoryId(rs.getInt("category_id"));
				bookList.add(book);
			}
			System.out.println("searchBook completed");
			return bookList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品総数を取得
	 *
	 * @param searchWord
	 * @return
	 * @throws SQLException
	 */
	public static double getCount(String searchWord) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from book where book_title like ? OR author_name LIKE ?");
			st.setString(1, "%" + searchWord + "%");
			st.setString(2, "%" + searchWord + "%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//IDで書籍を検索する
	public static BookDataBeans findByBookID(int bookId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM book WHERE book_id = ?");
			st.setInt(1, bookId);

			ResultSet rs = st.executeQuery();
			BookDataBeans book = new BookDataBeans();

			if (rs.next()) {
				book.setId(rs.getInt("book_id"));
				book.setTitle(rs.getString("book_title"));
				book.setAuthorName(rs.getString("author_name"));
				book.setPublisher(rs.getString("publisher"));
				book.setPublishYear(rs.getString("publish_year"));
				book.setDetail(rs.getString("detail"));
				book.setFileName(rs.getString("file_name"));
				book.setCategoryId(rs.getInt("category_id"));

			}
			System.out.println("findByBookID completed");
			return book;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

}
