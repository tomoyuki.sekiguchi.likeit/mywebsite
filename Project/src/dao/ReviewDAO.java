package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ReviewDataBeans;

public class ReviewDAO {

	public static void insertReviewData(int fId, int userId,String review) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO review_book(favorite_id,user_id,review) VALUES(?,?,?)");
			st.setInt(1, fId);
			st.setInt(2, userId);
			st.setString(3,review);
			st.executeUpdate();

			System.out.println("insertReviewData completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static List<ReviewDataBeans> findById(int userId)throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			List<ReviewDataBeans> reviewList = new ArrayList<ReviewDataBeans>();
			//ここでテーブル結合する？
			st = con.prepareStatement("SELECT * FROM review_book"
					+ " JOIN favorite_book"
					+ " ON review_book.favorite_id = favorite_book.favorite_id"
					+ " JOIN book"
					+ " ON favorite_book.book_id = book.book_id"
					+ " WHERE review_book.user_id = ?");

			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();

			//レヴュー情報も取得したい。
			//新たなBeansが必要？？
			while (rs.next()) {
				ReviewDataBeans rdb = new ReviewDataBeans();
				rdb.setUserId(rs.getInt("user_id"));
				rdb.setBookId(rs.getInt("book_id"));
				rdb.setFavoriteId(rs.getInt("favorite_id"));
				rdb.setTitle(rs.getString("book_title"));
				rdb.setAuthorName(rs.getString("author_name"));
				rdb.setPublisher(rs.getString("publisher"));
				rdb.setPublishYear(rs.getString("publish_year"));
				rdb.setDetail(rs.getString("detail"));
				rdb.setFileName(rs.getString("file_name"));
				rdb.setCategoryId(rs.getInt("category_id"));
				rdb.setReview(rs.getString("review"));

				reviewList.add(rdb);
			}

			System.out.println("Find ReviewList completed");

			return reviewList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

}
